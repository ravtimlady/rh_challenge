class PopulateAdmin < ActiveRecord::Migration[5.1]
  def change
    User.find_or_create_by(email: 'admin@teste.com') do |user|
      user.password = 'senha_admin'
      user.admin = true
    end
  end
end
