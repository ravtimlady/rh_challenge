# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Create admin user
User.create(email: 'admin@teste.com', password: 'senha_admin', admin: true)
User.create(email: 'user@teste.com', password: 'senha_user', admin: false)

Workspace.create(title: 'Rocinante')
Workspace.create(title: 'Bala no Alvo')
Workspace.create(title: 'Aldebaran')
Workspace.create(title: 'Rapidash')
Workspace.create(title: 'Sleipnir')

5.times do
  JobRole.create(title: Faker::Job.unique.title)
end

10.times do
  workspace = Workspace.all.sample
  job_role = JobRole.all.sample

  Person.create(
    description: Faker::Name.name,
    registration: Faker::Number.unique.number(digits: 6).to_s,
    birth_date: Faker::Date.birthday(min_age: 18, max_age: 65),
    birth_county: Faker::Address.city,
    birth_state: Faker::Address.state,
    marital_status: Faker::Demographic.marital_status,
    sex: Faker::Gender.binary_type,
    workspace: workspace,
    job_role: job_role
  )
end