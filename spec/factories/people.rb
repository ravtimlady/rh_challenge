FactoryBot.define do
  factory :person do
    description { Faker::Name.name }
    registration { Faker::Number.unique.number(digits: 6).to_s }
    workspace
    job_role
  end
end