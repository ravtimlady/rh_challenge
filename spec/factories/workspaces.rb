FactoryBot.define do
  factory :workspace do
    title { Faker::Company.name }
  end
end