FactoryBot.define do
  factory :job_role do
    title { Faker::Job.title }
  end
end