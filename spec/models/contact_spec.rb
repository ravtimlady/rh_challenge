# spec/models/contact_spec.rb
require 'rails_helper'
require 'faker'

RSpec.describe Contact, type: :model do
  describe "associations" do
    it { should belong_to(:person) }
  end

  describe "validations" do
    let(:person) { Person.create(description: Faker::Name.name, registration: Faker::Number.unique.number(digits: 6)) }

    subject do
      Contact.new(
        telephone: Faker::PhoneNumber.phone_number,
        cell_phone: Faker::PhoneNumber.cell_phone,
        email: Faker::Internet.unique.email,
        person: person
      )
    end

    it { should validate_presence_of(:telephone) }
    it { should validate_presence_of(:cell_phone) }
    it { should validate_presence_of(:email) }

    it { should validate_length_of(:telephone).is_at_most(13) }
    it { should validate_length_of(:cell_phone).is_at_most(14) }

    it { should validate_uniqueness_of(:telephone).scoped_to(:person_id).case_insensitive }
    it { should validate_uniqueness_of(:cell_phone).scoped_to(:person_id).case_insensitive }
  end
end
