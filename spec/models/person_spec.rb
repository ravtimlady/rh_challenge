# spec/models/person_spec.rb
require 'rails_helper'
require 'faker'

RSpec.describe Person, type: :model do
  describe "associations" do
    it { is_expected.to belong_to(:workspace).inverse_of(:person) }
    it { is_expected.to belong_to(:job_role).inverse_of(:person) }
    it { is_expected.to have_many(:contacts) }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_presence_of(:registration) }
    it { is_expected.to validate_uniqueness_of(:registration) }
    it { is_expected.to validate_length_of(:registration).is_equal_to(6) }
    it { is_expected.to validate_numericality_of(:registration).only_integer }
  end

  describe "uniqueness of job_role_id within workspace scope" do
    subject { Person.create(description: Faker::Name.name, registration: Faker::Number.number(digits: 6), workspace: workspace_instance, job_role: job_role_instance) }

    let(:workspace_instance) { Workspace.create(title: Faker::Company.name) }
    let(:job_role_instance) { JobRole.create(title: Faker::Job.title) }

    it { is_expected.to validate_uniqueness_of(:job_role_id).scoped_to(:workspace_id) }
  end

  describe "nested attributes" do
    it { is_expected.to accept_nested_attributes_for(:contacts).allow_destroy(true) }
  end
end
